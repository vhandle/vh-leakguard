VH_LeakGuard = { }

VH_LeakGuard.Initial = false

VH_LeakGuard.DeniedBody = "noway"

VH_LeakGuard.Queue = { }

VH_LeakGuard.ReturnFuncs = { }

VH_LeakGuard.ReturnFuncs.DefaultInclude = function ( self, File, Body )

	if Body == VH_LeakGuard.DeniedBody then

		MsgC( Color( 255, 0, 0 ), "Please insert a valid leakguard id for the addon " .. self.Name .. "\n" )

		timer.Create( File .. "LID", 60, 0, function ( )

			if self.Included[ File ] then

				timer.Remove( File .. "LID" )

				return

			end

			MsgC( Color( 255, 0, 0 ), "Please insert a valid leakguard id for the addon " .. self.Name .. "\n" )

		end )

		return

	end

	self.Included[ File ] = true

	RunStringEx( Body, File .. "-LeakGuard" )

end

VH_LeakGuard.ReturnFuncs.SingleMsgInclude = function ( self, File, Body )

	if Body == VH_LeakGuard.DeniedBody then

		MsgC( Color( 255, 0, 0 ), "Please insert a valid leakguard id for the addon " .. self.Name .. "\n" )

		return

	end

	self.Included[ File ] = true

	RunStringEx( Body, File .. "-LeakGuard" )

end

VH_LeakGuard.ReturnFuncs.NoMsgInclude = function ( self, File, Body )

	if Body == VH_LeakGuard.DeniedBody then

		return

	end

	self.Included[ File ] = true

	RunStringEx( Body, File .. "-LeakGuard" )

end

function VH_LeakGuard.NewAddon( Name, LeakGuardID )

	local Object = { Name = ( Name or "LeakGuard incorrectly setup" ), LeakGuardID = ( LeakGuardID or "0" ), Included = { } }

	function Object:FetchFile( File, Version, ReturnFunc )

		if self.Included[ File ] then return end

		local LeakGuardID = ( type( self.LeakGuardID ) ~= "function" and self.LeakGuardID or self:LeakGuardID( File, Version ) ) or "0"

		HTTP( { url =  "http://www.v-handle.com/jcstats?version=" .. Version .. "&leakguard_id=" .. LeakGuardID, success = function ( Code, Body ) ReturnFunc( self, File, Body ) end } )

	end

	function Object:IncludeFile( File, Version, ReturnFunc )

		ReturnFunc = ReturnFunc or VH_LeakGuard.ReturnFuncs.DefaultInclude

		if VH_LeakGuard.Initial then

			self:FetchFile( File, Version, ReturnFunc )

		else

			table.insert( VH_LeakGuard.Queue, { LeakGuardAddon = self, File = File, Version = Version, ReturnFunc = ReturnFunc } )

		end

	end

	return Object

end

hook.Add( "Think", "vh-leakguard-initial", function ( )

	VH_LeakGuard.Initial = true

	for a, b in ipairs( VH_LeakGuard.Queue ) do

		b.LeakGuardAddon:FetchFile( b.File, b.Version, b.ReturnFunc )

		VH_LeakGuard.Queue[ a ] = nil

	end

	hook.Remove( "Think", "vh-leakguard-initial" )

end )
